"""
    A simulator for multiple applications running with same MBTF (Mean Time Between Failure).
    Failure distribute in exponential distribution with MTBF equal to 10.68 hour.
    By applying different scheduling algorithm, we can get the throughput of the system.
"""

import random
import pylab
import math
import numpy as np
import scipy as sp

""" 
    Get check point interval according to: 
        OCI = sqrt(2 * CheckPointDuration * MTBF)
"""
def getCheckPointInterval(CP,MTBF):
    return float("{0:.2f}".format(math.sqrt(2*CP*MTBF)))

"""
    Get check point duration
"""
def getCheckPointDuration(n):
    return float("{0:.2f}".format(n*0.1+0.1))

"""
    Get recovery time according to:
        Recovery = CheckPointDuration/2
"""
def getRecoveryTime(CP):
    return CP/2

"""
    Get error interval which distributed as Exponential distribution
"""
def getErrorTime(MTBF):
    g = random.random()
    expo = np.random.exponential(scale=MTBF)
    return expo

"""
    Get start time of each application
"""
def getStartTime(exe_each,start,N):
    for i in range(N):
        if exe_each[i]==0:
            start[i] = sum(exe_each)
    return start

"""
    Get finish time of each application. When compute time of each application exceeds setted value, return current execution time
"""
def getDepartTime(S,CheckPointInterval,compute,depart,control,N):
    for n in range(N):
        if S[n]*CheckPointInterval[n]>compute[n] and depart[n]==0:
            print "log:S[n]*CheckPointInterval[n]>compute[n]:",S[n]*CheckPointInterval[n]>compute[n]
            depart[n] = exe_total
            control[n] = 'no'
    print "log: depart:",depart
    return depart

"""
    Get wait time of each application
"""
def getWaitTime(depart,exe_eachjob,N):
    for n in range(N):
        wait[n] = depart[n] - exe_eachjob[n]
    return wait

"""
    Get useful computation time
"""
def getUsefulComputation(S,CheckPointInterval,compute):
    sum = 0
    each = 0
    for n in range(N):
        each = S[n]*CheckPointInterval[n]
        if each >= compute[n]:
            each = compute[n]
        sum += each
    return sum

"""
    Get flag as a control bit so that we can choose next application to run.
"""
def getFlag(app,N,control,flag):
    flag = (app+1)%N
    while control[flag]=='no':
        flag = (flag+1)%N
    return flag

"""
    Round Robin Scheduler
"""
def getApp(flag,control,S,i,compute,N):
    return flag

"""
    Sequential Scheduler
"""
#def getApp(flag,control,S,i,compute,N):
#    print S,i,compute
#    for n in range(N):
#        if S[n]<compute[n]/i[n]:
#            return n


N = 10                                        # numbers of applications
S = []                                        # number of completion of computation
F = 0                                         # numbers of failures
start = []
depart = []
wait = []
duration = []
flag = 0                                      # control bit for RR Scheduler
CP = 0.0
RC = 0.0
OCI = 0.0
MTBF = 10.68
compute = []
UsefulComput = 0
CheckPoint = []
Recovery = []
CheckPointInterval = []
exe_total = 0.0
exe_eachjob = []                               # initialization
control = []
for j in range(5):
    for n in range(N):
        control.append(n)
        exe_eachjob.append(0)
        S.append(0)
        start.append(0)
        depart.append(0)
        wait.append(0)
        duration.append(0)
        compute.append(200)
        CP = getCheckPointDuration(n)
        RC = getRecoveryTime(CP)
        OCI = getCheckPointInterval(CP,MTBF)
        CheckPoint.append(CP)
        Recovery.append(RC)
        CheckPointInterval.append(OCI)
        print "CP: {0}, RC: {1}, OCI: {2}".format(CP,RC,OCI)
    compute_total = sum(compute)
    random.seed()
    app = 0
    checkpoint = CheckPoint[0]
    recovery = Recovery[0]
    checkpointinterval = CheckPointInterval[0]
    circle = checkpointinterval+checkpoint
    erroroccur = 0.0
    temp = 0
    while(UsefulComput<compute_total):
        errorinterval = getErrorTime(MTBF)                        # get next error interval
        erroroccur += errorinterval
        print "Next errorinterval is:",errorinterval
        print "Next error occurs at:",erroroccur
        F += 1
        app_temp = getApp(flag,S,control,CheckPointInterval,compute,N)
        print "outloop :",app_temp
        checkpointinterval = CheckPointInterval[app_temp]
        recovery = Recovery[app_temp]
        if errorinterval<recovery and F>=2:                       # error occurs during recovery
            temp = erroroccur
            continue
        if errorinterval>recovery and F>=2:                       # error occurs after recovery
            exe_total = temp + recovery
            
            exe_eachjob[app_temp] = exe_total - (sum(exe_eachjob)-exe_eachjob[app_temp])
        
        while(erroroccur-exe_total>circle and UsefulComput<compute_total):
            print "Before simulation execution of each job is :",exe_eachjob
            app = getApp(flag,control,S,CheckPointInterval,compute,N)
            print "inloop :",app
            checkpointinterval = CheckPointInterval[app]
            recovery = Recovery[app]
            checkpoint = CheckPoint[app]
            circle = checkpointinterval+checkpoint
            exe_eachjob[app] += circle
            print "After simulation execution of each job is :",exe_eachjob
            S[app] += 1
            exe_total = sum(exe_eachjob)
            start = getStartTime(exe_eachjob,start,N)
            depart = getDepartTime(S,CheckPointInterval,compute,depart,control,N)
            
            UsefulComput = getUsefulComputation(S,CheckPointInterval,compute)
            if UsefulComput==compute_total:
                break
            print "useful compute",UsefulComput
            
            flag = getFlag(app,N,control,flag)
            

        wait = getWaitTime(depart,exe_eachjob,N)
        for i in range(N):
            duration[i] = depart[i]-start[i]
        temp = erroroccur
        
        print "Before next error, now we have useful compute time ===",UsefulComput,"=== and total execution time",exe_total

print "CP: {0}\nRC: {1}\nOCI: {2}\n".format(CheckPoint,Recovery,CheckPointInterval)
print "compute time:",compute
print "execute time:",exe_eachjob
print "number of failures:",F
print "number of useful computation:",S
print "throughput:",N/exe_total
print "start time:",start
print "depart time:",depart
print "wait time:",wait
print "Duration time:",duration