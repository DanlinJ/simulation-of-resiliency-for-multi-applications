This is a simulator for multiple applications running with same MBTF (Mean Time Between Failure). 
Failure distribute in exponential distribution with MTBF equal to 10.68 hour. 
By applying different scheduling algorithm, we can get the throughput of the system.

